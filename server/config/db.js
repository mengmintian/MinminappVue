/**
 * mysql数据库配置信息
 */

module.exports = {
	mysql: {
		host     : 'localhost',
		user     : 'root',
		password : 'root',
		database : 'minminapp',
		port : 3306
	}
};
